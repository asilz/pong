#include <Ball.h>

Ball::Ball(int _velocityY,TDT4102::Color ballColor ,AnimationWindow& win){
    velocityX = 0;
    velocityY = _velocityY;
    color = ballColor;
    posX = win.width()/2;
    posY = win.height()/2;
}

void Ball::move(){
    this->posX += velocityX;
    this->posY += velocityY;
}

void Ball::draw(AnimationWindow& win){
    win.draw_circle(TDT4102::Point{static_cast<int>(posX), posY}, radius, this->color);
}

void Ball::bounce(Player player1, Player player2, AnimationWindow &win){
    if(this->posX < player1.getX()+width/2+radius && this->posX > player1.getX()-width/2-radius && this->posY < player1.getY()+height/2+radius && this->posY > player1.getY()-height/2-radius && velocityY>0){
        velocityY = -velocityY;
        velocityX = (this->posX-player1.getX())/12;
    }
    if(this->posX < player2.getX()+width/2+radius && this->posX > player2.getX()-width/2-radius && this->posY < player2.getY()+height/2+radius && this->posY > player2.getY()-height/2-radius&&velocityY<0){
        velocityY = -velocityY;
        velocityX = (this->posX-player2.getX())/12;
    }
    if(posX <= 0 || posX >= win.width()){
        velocityX = -velocityX;
    }   
}

double Ball::getX(){
    return this->posX;
}

int Ball::getY(){
    return this->posY;
}

void Ball::reset(AnimationWindow &win){
    posX = win.width()/2;
    posY = win.height()/2;
    velocityX = 0;
}