#include <Ball.h>

int main() {
    AnimationWindow win = AnimationWindow(50,50,800,600);
    Player player1 = Player(true, Color::green, win);
    Player player2 = Player(false, Color::blue, win);
    Ball ball = Ball(2, Color::red, win);
    while(player1.getScore()<10 && player2.getScore()<10){
        player1.move(win);
        player2.move(win);
        ball.bounce(player1,player2,win);
        ball.move();
        player1.draw(win);
        player2.draw(win);
        ball.draw(win);
        if(ball.getY()<0){
            ball.reset(win);
            player1.victory();
        }
        if(ball.getY()>win.height()){
            ball.reset(win);
            player2.victory();
        }
        win.next_frame();
    }
    win.wait_for_close();
    return 0;
}

