#include <Player.h>
#include <Ball.h>

Player::Player(bool isUnder, TDT4102::Color playerColor, AnimationWindow& win){
    posX = win.width()/2;
    if(isUnder){
        posY = win.height()-20+(1+height/2);
    }
    else{
        posY = 20+(1+height/2);
    }
    color = playerColor;
    under = isUnder;
    score = 0;
    Network network = Network();
}

void Player::move(AnimationWindow& win){
    if(under){
        this->posX = win.get_mouse_coordinates().x;
    }
}

void Player::networkMove(Ball ball){
    network.inputs.at(1) = ball.getX();
    network.inputs.at(2) = ball.getY();
    network.updateZ();
    network.updateWeight();
    network.updateBias();
    network.predict();
    network.getOutput();
}

void Player::draw(AnimationWindow& win){
    win.draw_rectangle(TDT4102::Point{posX-(width/2),posY-(1+height/2)}, width, height, this->color);
    win.draw_text(TDT4102::Point{posX,posY-(1+height)},to_string(score), TDT4102::Color::black,10U);
}

int Player::getX(){
    return this->posX;
}

int Player::getY(){
    return this->posY;
}

int Player::getScore(){
    return this->score;
}

void Player::victory(){
    score++;
}