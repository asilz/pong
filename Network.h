#include <AnimationWindow.h>
#include <std_lib_facilities.h>

class Network{
    private:
        double output;
        std::vector<double> weights;
        int bias;
        int z;
    public:
        std::vector<int> inputs;
        Network();
        int sum();
        void updateWeight();
        void updateBias();
        void updateZ();
        double sigmoid();
        void predict();
        double getOutput();
};