#include <Network.h>


int constexpr width = 40;
int constexpr height = 12;

class Player{
    private:
        int posX;
        int posY;
        int score;
        bool under;
        TDT4102::Color color;
    public:
        void draw(AnimationWindow& win);
        void move(AnimationWindow& win);
        Player(bool isHuman, TDT4102::Color playerColor, AnimationWindow& win);
        int getX();
        int getY();
        int getScore();
        void victory();
        Network network;
        void networkMove(Ball ball);

};