#include <Network.h>

#define e 2.718281828459045
#define learnRate 1.0

Network::Network(){
    inputs = std::vector<int>(3, 1); //input 0 is playerPosX // input 1 is ballPosX // input 2 is ballPosY
    weights = std::vector<double>(3, 1.0);
    output = -10;
    bias = 0;
    z = 0;
}

int Network::sum(){
    2*(10-output);
}

void Network::updateZ(){
    z = 0;
    for(int i = 0; i<inputs.size(); i++){
        z+=inputs.at(i)*weights.at(i);
    }
}

double Network::sigmoid(){
    1/(1+pow(e,-z));
}

void Network::updateWeight(){
    for(int i = 0; i<weights.size(); i++){
        weights.at(i) = weights.at(i) - learnRate*this->sum()*sigmoid()*(1-sigmoid())*inputs.at(i);
    }
}

void Network::updateBias(){
    bias = bias - learnRate*this->sum()*sigmoid()*(1-sigmoid());
}

void Network::predict(){
    output = sigmoid();
}

double Network::getOutput(){
    return output;
}

