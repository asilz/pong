
#include <Player.h>

constexpr int radius = 5;

class Ball{
    private:
        double posX;
        int posY;
        double velocityX;
        int velocityY;
        TDT4102::Color color;
        
    public:
        void move();
        void draw(AnimationWindow& win);
        void bounce(Player player1, Player player2, AnimationWindow &win);   
        Ball(int velocityY,TDT4102::Color ballColor ,AnimationWindow& win);
        double getX();
        int getY();
        void reset(AnimationWindow &win);
};